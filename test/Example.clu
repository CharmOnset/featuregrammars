<?xml version="1.0" encoding="UTF-8"?>
<!-- A Feature Grammar -->

<feature-grammar  start="document">
    <!-- We can use the parameter where we have outside knowledge to override the rules -->
    <param name="force-AU" select="false()"  />
    <param name="force-NZ" select="false()"  />
    
    <feature name="document" model="AU-document | NZ-document" note="root">
        <find in="input" match="/document"/>
        <find in="output" match="/book"/>
    </feature>
    
    <feature name="AU-document" model="AU-head,  body, coda?">
        <alias lang="en">australian-sourced document</alias>
        <find in="input" match="($force-AU and not($force-NZ)) or document[@country='AU']  "/>
        <find in="output" match="/book/"/>
    </feature>
    
    <feature name="NZ-document" model=" NZ-head, (NZ-body | body)">
        <alias lang="en">New Zealand -sourced document</alias>
        <find in="input" match="$force-NZ or /document[@country='NZ']"/>
        <find in="output" match="/book/"/>
    </feature>
    
    <feature name="AU-head" model="#EMPTY">
        <find in="input" match="/document/head"/>
        <find in="output" match="/book/"/>
    </feature>
    
    <feature name="NZ-head" model="#EMPTY">
        <find in="input" match="/document/head"/>
        <find in="output" match="/book/"/>
    </feature>
    
    <feature name="body" model="formal-para, informal-para? ">
        <find in="input" match="/*"/>
        <find in="output" match="/book/"/>
    </feature>
    
    <feature name="NZ-body" model="formal-para | informal-para ">
        <find in="input" match="/document/body//level[starts-with(@id, 'NZ')]"/>
        <find in="output" match="/book/"/>
    </feature>
    
    <feature name="formal-para" model="#EMPTY">
        <find in="input" match="/document/body//p[@class='p0']"
            >A formal paragraph is a p element with a class attribute of "p0" </find>
        <find in="output" match="/book/"/>
    </feature>
    
    <feature name="informal-para" model="#EMPTY">
        <find in="input" match="/document/body//p[not(@class='p0')][not(ancestor::p)]"
            >An informal paragraph is a p element that is not marked up as a p0 and is not inside another p element.</find>
        <find in="output" match="/book/"/>
    </feature>
    
    
    <feature name="coda" model="#EMPTY">
        <find in="input" match="/document/body//footnote"/>
        <find in="output" match="/book/"/>
    </feature>
    
</feature-grammar>
