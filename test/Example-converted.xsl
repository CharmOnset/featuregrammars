<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:clu="RICK"
    xmlns:saxon="http://saxon.sf.net/"
    version="2.0">
    <xsl:param name="force-AU" select="false()"/>
    <xsl:param name="force-NZ" select="false()"/>
    <!--These variables select whether each feature was found or not-->
    <xsl:variable name="document-find" select="/document"/>
    <xsl:variable name="AU-document-find"
        select="($force-AU and not($force-NZ)) or document[@country='AU']  "/>
    <xsl:variable name="NZ-document-find" select="$force-NZ or /document[@country='NZ']"/>
    <xsl:variable name="AU-head-find" select="/document/head"/>
    <xsl:variable name="NZ-head-find" select="/document/head"/>
    <xsl:variable name="body-find" select="/*"/>
    <xsl:variable name="NZ-body-find"
        select="/document/body//level[starts-with(@id, 'NZ')]"/>
    <xsl:variable name="formal-para-find" select="/document/body//p[@class='p0']"/>
    <xsl:variable name="informal-para-find"
        select="/document/body//p[not(@class='p0')][not(ancestor::p)]"/>
    <xsl:variable name="coda-find" select="/document/body//footnote"/>
    <!--These rules evaluates the features according to the cluster grammar-->
    <xsl:template match="/">
        <feature-plan>
            <xsl:choose>
                <xsl:when test="$AU-document-find">
                    <AU-document>
                        <xsl:choose>
                            <xsl:when test="$AU-head-find">
                                <AU-head> </AU-head>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:message>Required feature "AU-head" not found</xsl:message>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:choose>
                            <xsl:when test="$body-find">
                                <body>
                                    <xsl:choose>
                                        <xsl:when test="$formal-para-find">
                                            <formal-para> </formal-para>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:message>Required feature "formal-para" not found</xsl:message>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <xsl:choose>
                                        <xsl:when test="$informal-para-find">
                                            <informal-para>
                                            </informal-para>
                                        </xsl:when>
                                    </xsl:choose>
                                </body>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:message>Required feature "body" not found</xsl:message>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:choose>
                            <xsl:when test="$coda-find">
                                <coda>
                                </coda>
                            </xsl:when>
                        </xsl:choose>
                    </AU-document>
                </xsl:when>
                <xsl:when test="$NZ-document-find">
                    <NZ-document>
                        <xsl:choose>
                            <xsl:when test="$NZ-head-find">
                                <NZ-head> </NZ-head>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:message>Required feature "NZ-head" not found</xsl:message>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:choose>
                            <xsl:when test="$NZ-body-find">
                                <NZ-body>
                                    <xsl:choose>
                                        <xsl:when test="$formal-para-find">
                                            <formal-para> </formal-para>
                                        </xsl:when>
                                        <xsl:when test="$informal-para-find">
                                            <informal-para> </informal-para>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:message>Choice not found</xsl:message>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </NZ-body>
                            </xsl:when>
                            <xsl:when test="$body-find">
                                <body>
                                    <xsl:choose>
                                        <xsl:when test="$formal-para-find">
                                            <formal-para> </formal-para>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:message>Required feature "formal-para" not found</xsl:message>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <xsl:choose>
                                        <xsl:when test="$informal-para-find">
                                            <informal-para>
                                            </informal-para>
                                        </xsl:when>
                                    </xsl:choose>
                                </body>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:message>Choice not found</xsl:message>
                            </xsl:otherwise>
                        </xsl:choose>
                    </NZ-document>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:message>Choice not found</xsl:message>
                </xsl:otherwise>
            </xsl:choose>
        </feature-plan>
    </xsl:template>
</xsl:stylesheet>
