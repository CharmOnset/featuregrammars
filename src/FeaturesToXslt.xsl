<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    FeaturesToXslt.xsl  - implementation of Feature Grammars
    
        Compile a feature grammar into an XSLT script.  
        The "family" parameter can be used to select alternative find rules for different input document types.
    
    Original: 2016-10-07  Rick Jelliffe
    
    
    TODO: 
      * Recursive Grammars will crash: put in guards
      * Put in XPath and model validation
      * Implement name aliases
      * Should transport document name as part of top-level element, if enabled by parameter
-->
<!-- 
    Rick Jelliffe (C) 2016
    Artistic License 2.0  https://opensource.org/licenses/Artistic-2.0
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:clu="https://bitbucket.org/CharmOnset/featuregrammars/" xmlns:saxon="http://saxon.sf.net/"
    xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias"
    exclude-result-prefixes="xs" version="3.0">
    
    <xsl:namespace-alias stylesheet-prefix="axsl" result-prefix="xsl"/>
    <xsl:param name="family" select="'input'"/>
    <xsl:output indent="yes"/>

    <!-- ========= MAIN =========================-->


    <xsl:variable name="decorated-grammar">
        <feature-grammar>
            <xsl:attribute name="start" select="/feature-grammar/@start"/>
            <xsl:apply-templates select="/feature-grammar/feature" mode="decorate-grammar"/>
        </feature-grammar>
    </xsl:variable>


    <xsl:template match="/feature-grammar">
        <axsl:stylesheet  version="2.0">

            <xsl:for-each select="param">
                <axsl:param>
                    <xsl:copy-of select="node() | @*"/>
                </axsl:param>
            </xsl:for-each>

            <!-- TODO validate that each model has a matching number of ( and ).
                And that there is no } before the first {, and no { after the last } 
            That will make the text parsing safer.  -->

            <xsl:comment>These variables select whether each feature was found or not</xsl:comment>
            <xsl:for-each select="feature">
                <axsl:variable name="{concat( @name,  '-find')}" select="{find[@in = $family]/@match}"/>
            </xsl:for-each>

            <xsl:comment>These rules evaluates the features according to the feature-grammar grammar</xsl:comment>
            
                <!--
                <xsl:copy-of select="$decorated-grammar" />
                -->

                <xsl:apply-templates
                    select="$decorated-grammar/feature-grammar/feature[@name=/feature-grammar/@start]"
                    mode="generate-grammar"/>
         

        </axsl:stylesheet>

    </xsl:template>

    <!-- ============ LEXICAL ====================== -->

    <xsl:function name="clu:tokenize" as="xs:string*">
        <xsl:param name="string"/>
        <!-- This function tokenizes a feature-grammar model into tokens, where including tokens for , | ( )  but forcing the 
            ?  to attach to the previous token. It returns a sequence of tokens, suitavle for parsing. -->
        <xsl:sequence
            select="tokenize( 
             replace(
                normalize-space( 
                    replace (
                        replace (
                            replace (
                                replace (
                                    $string,
                                    '\|',
                                    ' | '
                                ),
                            ',',
                            ' , '
                          ),
                          '\(',
                          ' ( '    
                        ),
                     '\)',
                     ' ) '
                     ) 
                ),
             ' \?', 
             '?' 
            ), 
            ' ' ) "
        />
    </xsl:function>


    <!-- Decorate the incoming grammar with tokenized version of the grammar -->
    <xsl:template match="feature" mode="decorate-grammar">

        <xsl:variable name="token-list" as="element()*">
            <xsl:for-each select="clu:tokenize(@model)">
                <token>
                    <xsl:value-of select="."/>
                </token>
            </xsl:for-each>
        </xsl:variable>

        <xsl:variable name="grouped-token-list" as="element()*">
            <xsl:choose>
                <xsl:when test="$token-list[1] = '#EMPTY' ">
                    <token>#EMPTY</token>
                </xsl:when>
                <xsl:otherwise>

                    <token>(</token>
                    <xsl:copy-of select="$token-list"/>
                    <token>)</token>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:copy>
            <xsl:copy-of select="node() | @* "/>

            <token-list>
                <xsl:copy-of select="$grouped-token-list"/>
            </token-list>



        </xsl:copy>
    </xsl:template>


    <!-- ==============  REWRITE ====================== -->

    <xsl:template match="grammar" mode="rewrite-grammar">

            <xsl:apply-templates mode="rewrite-grammar"/>
        
    </xsl:template>

    <xsl:template match="group" mode="rewrite-grammar">
        <xsl:choose>
            <xsl:when test="or">
                <choice>
                    <xsl:if test="following-sibling::*[1][self::optional]">
                        <xsl:attribute name="optional">yes</xsl:attribute>
                    </xsl:if>
                    <xsl:apply-templates select="*[not(self::or)][not(self::optional)]"  mode="rewrite-grammar"/>
                </choice>
            </xsl:when>
            <xsl:when test="and">
                <sequence>
                    <xsl:if test="following-sibling::*[1][self::optional]">
                        <xsl:attribute name="optional">yes</xsl:attribute>
                    </xsl:if>
                    <xsl:apply-templates select="*[not(self::and)][not(self::optional)]" mode="rewrite-grammar"/>
                </sequence>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="*[not(self::optional)]" mode="rewrite-grammar"/>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>
    
    <xsl:template match="element" mode="rewrite-grammar"> 
        <xsl:copy>
            <xsl:copy-of  select="@*"/>
            <xsl:apply-templates mode="rewrite-grammar" />
        </xsl:copy>
        
    </xsl:template>


    <xsl:template match="empty" mode="rewrite-grammar"> 
        <empty/>
    </xsl:template>
    
    
    <!-- ==============  GENERATE XSLT ====================== -->
    
    <xsl:template match="grammar" mode="generate-xslt"> 
        <axsl:template match="/">
            <xsl:apply-templates mode="generate-xslt"/>
        </axsl:template>
    </xsl:template>
    
    <xsl:template match="choice" mode="generate-xslt"> 
        <axsl:choose>
           <xsl:for-each select="*" >
               <xsl:apply-templates select="."  mode="generate-xslt" />
           </xsl:for-each>
            
            <xsl:if test="not(@optional='yes')">
               <axsl:otherwise>
                   <axsl:message>None of the required choices of feature "<xsl:value-of select="ancestor::element[1]/@name" />" were found</axsl:message>
               </axsl:otherwise>
            </xsl:if>
        </axsl:choose> 
    </xsl:template>
    
    <xsl:template match="sequence" mode="generate-xslt"> 
        <xsl:apply-templates mode="generate-xslt" />
    </xsl:template>
    
    <xsl:template match="sequence/element" mode="generate-xslt"> 
        <axsl:choose>
            <axsl:when  test="${@name}-find">
              <xsl:element name="{@name}">
                  <xsl:apply-templates mode="generate-xslt" />
              </xsl:element>
            </axsl:when>
            <xsl:if test="not(@optional='yes')">
            <axsl:otherwise>
                
                <axsl:message>Required feature "<xsl:value-of select="@name"/>" not found</axsl:message>
            </axsl:otherwise>
            </xsl:if>
        </axsl:choose>
    </xsl:template>
    
    
    <xsl:template match="choice/element" mode="generate-xslt"> 
        <axsl:when  test="${@name}-find">
            <xsl:element name="{@name}">
                <xsl:apply-templates mode="generate-xslt" />
            </xsl:element>
        </axsl:when>
    </xsl:template>
    
    <xsl:template match="empty" mode="generate-xslt"/> 
    
    
    <!-- ==============  GRAMMAR ====================== -->

    <!--
        start ::= "#EMPTY" | model
        model ::= ( term (( :,: term )* | ( “|” term)*) ) 
        term ::= ( “(“  model  “) “   “?”?) |  ( name “?”? )
        
        -->


    <xsl:function name="clu:translate-tokens-to-xslt" as="xs:string*">
        <xsl:param name="model-name" as="xs:string"/>
        <xsl:param name="grammar" as="element()"/>
        <xsl:param name="context" as="xs:string*"/>

        <xsl:for-each select="$grammar/feature[@name= $model-name]/token-list/token">
            <xsl:choose>
                <xsl:when test=". = '#EMPTY'">
                    <xsl:text> <![CDATA[<empty/>]]></xsl:text>
                </xsl:when>
                <xsl:when test=". = '('">
                    <xsl:text><![CDATA[<group>]]></xsl:text>
                </xsl:when>

                <xsl:when test=". = ')'">
                    <xsl:text><![CDATA[</group>]]></xsl:text>
                </xsl:when>

                <xsl:when test=". = ')?'">
                    <xsl:text><![CDATA[</group><optional/>]]></xsl:text>
                </xsl:when>

                <xsl:when test=". = ','">
                    <xsl:text><![CDATA[<and/>]]></xsl:text>
                </xsl:when>

                <xsl:when test=". = '|'">
                    <xsl:text><![CDATA[<or/>]]></xsl:text>
                </xsl:when>

                <xsl:when test="ends-with(., '?')">
                    <xsl:text>&lt;element name="</xsl:text>
                    <xsl:value-of select="substring-before(., '?')"/>
                    <xsl:text>" optional="yes"></xsl:text>
                    <xsl:choose>
                        <xsl:when test="substring-before(., '?')  = $grammar/feature/@name">
                            <xsl:text>&#x0A;</xsl:text>
                            <xsl:text><![CDATA[<test name="]]></xsl:text>
                            <xsl:value-of select="substring-before(., '?')"/>
                            <xsl:text>"></xsl:text>
                            <xsl:copy-of
                                select="clu:translate-tokens-to-xslt(substring-before(., '?'), $grammar,  concat( $context, '/', $model-name ))"/>
                            <xsl:text><![CDATA[</test>]]></xsl:text>
                        </xsl:when>

                        <xsl:otherwise>
                            <xsl:message terminate="yes">Unexpected token: "<xsl:value-of select="."
                                />"</xsl:message>
                        </xsl:otherwise>

                    </xsl:choose>
                    <xsl:text>&lt;/element> </xsl:text>

                </xsl:when>
                <xsl:when test=". = $grammar/feature/@name">
                    <xsl:text>&#x0A;</xsl:text>
                    <xsl:text>&lt;element name="</xsl:text>
                    <xsl:value-of select="."/>
                    <xsl:text>"></xsl:text>
                    <xsl:copy-of
                        select="clu:translate-tokens-to-xslt(., $grammar,  concat( $context, '/', $model-name ))"/>
                    <xsl:text>&lt;/element> </xsl:text>
                </xsl:when>

                <xsl:otherwise>
                    <xsl:message terminate="yes">Unexpected token: "<xsl:value-of select="."
                        />"</xsl:message>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>

    </xsl:function>


    <!-- 
        I don't care for this code much.  It is fragile to the input, generates tags, and uses a
        function not available in free products. Can improve by generating different verions of
        parse string. Want to avoid writing out to temp file too, or adding intermediate stage.
        So better to write an actual top-down parser?
    -->
    <xsl:template match="feature" mode="generate-grammar">
        <xsl:variable name="grammar">

        <grammar>
            <xsl:copy-of
                select="parse-xml(
                        string-join(
                            clu:translate-tokens-to-xslt(@name, $decorated-grammar/feature-grammar, '/' )))"
            />
        </grammar>
            
        </xsl:variable>
        <xsl:variable name="rewritten-grammar">
            <xsl:apply-templates select="$grammar"  mode="rewrite-grammar"   />
        </xsl:variable>
        
        <axsl:template match="/">
            <clu:features>
                <xsl:apply-templates select="$rewritten-grammar" mode="generate-xslt" />
            </clu:features>
        </axsl:template>        
    </xsl:template>


</xsl:stylesheet>
